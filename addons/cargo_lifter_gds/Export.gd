tool
extends EditorExportPlugin

var path: String

func _export_begin(_features: PoolStringArray, is_debug: bool, _path: String, _flags: int):
	var file: File = File.new()
	var json: String = ""

	if file.file_exists("res://cargo_lifter.json"):
		file.open("res://cargo_lifter.json", File.READ)
		json = file.get_as_text()
		file.close()
	else:
		json = "{\"path\": \"res://lib\"}"

	var parsed = JSON.parse(json)
	if typeof(parsed.result) != TYPE_DICTIONARY:
		print("ERROR! Invalid JSON format.")
		return

	path = parsed.result.get("path", "")
	if path.empty():
		print("ERROR! Invalid JSON configuration for the project path.")
		return
	var features: PoolStringArray = parsed.result.get("features", [])

	var rustflags: String = OS.get_environment("RUSTFLAGS")
	var args: PoolStringArray = parsed.result.get("rustc-flags")
	if not args.empty():
		rustflags += args.join(" ")
	if OS.get_name() != "Windows":
		OS.execute("export", ["RUSTFLAGS" + "=" + rustflags])
	else:
		OS.execute("set", ["RUSTFLAGS" + "=" + rustflags])

	var manifest: String = ""
	var uristart: bool = true
	if path.begins_with("res://"):
		manifest = path.substr(6)
	elif path.begins_with("user://"):
		var userdir: File = File.new()
		userdir.open(path, File.READ)
		manifest = userdir.get_path_absolute()
		userdir.close()
	else:
		manifest = path
		uristart = false

	if not manifest.ends_with("Cargo.toml"):
		if not OS.get_name() == "Windows" or uristart:
			manifest += "/Cargo.toml"
		else:
			manifest += "\\Cargo.toml"
	else:
		path = path.substr(0, -10)

	var build_args: Array = ["build", "--manifest-path", manifest]
	if not is_debug:
		build_args.append("--release")
	if not features.empty():
		build_args += ["--features", features.join(",")]

	var output: Array = []
	print(build_args)
	OS.execute("cargo", build_args, true, output, true)
	print(output)

func _export_end():
	pass

func _export_file(_path: String, _type: String, _features: PoolStringArray):
	pass
