tool
extends EditorPlugin

var exporter: EditorExportPlugin

func _enter_tree():
	exporter = preload("Export.gd").new()
	add_export_plugin(exporter)

func _exit_tree():
	remove_export_plugin(exporter)
