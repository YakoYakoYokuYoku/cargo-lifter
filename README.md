# Cargo-Lifter

A Godot Editor plugin for building your Rust projects alongside your game.

___

## Usage

Simply add the plugin into your `res://addons/` folder and provide a `res://cargo_lifter.json` configuration for your project.

Supported JSON keys are:
- `path`: **obligatory**, path to the project directory/`Cargo.toml` (a string describing the path with support for `res://` and `user://` URIs).
- `features`: chosen features for the crate (an array of strings each being a feature).
- `default-features`: toggle if the default features are desired or not (a boolean).
- `rustc-flags`: arguments for the `rustc` Rust Compiler (an array of strings each being an argument).

```json
{
  "path": "res://Lib",
  "features": ["websockets", "zlib"]
}
```
